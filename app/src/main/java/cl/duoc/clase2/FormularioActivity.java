package cl.duoc.clase2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import cl.duoc.clase2.bd.AlmacenDeFormularios;
import cl.duoc.clase2.entidades.Formulario;

public class FormularioActivity extends AppCompatActivity {

    private EditText etNombre, etRut, etEdad, etTelefono;
    private Button btnGuardar, btnLimpiar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario);

        etNombre = (EditText) findViewById(R.id.etNombre);
        etRut = (EditText) findViewById(R.id.etRut);
        etEdad = (EditText) findViewById(R.id.etEdad);
        etTelefono = (EditText) findViewById(R.id.etTelefono);

        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiarTextos();
            }
        });

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                guardarFormulario();
            }
        });
    }

    private void guardarFormulario() {
        String mensajeError = "";

        if(etNombre.getText().toString().length() < 1){
            mensajeError += "Ingrese nombre \n";
        }
        if(etTelefono.getText().toString().length()<12){
            mensajeError += "Ingrese teléfono según formato +569XXXXXXXX \n";
        }
        if(!isValidarRut(etRut.getText().toString())){
            mensajeError += "Ingrese un rut valido \n";
        }

        if(etEdad.getText().toString().length()>0){
            try{
                int edad = Integer.parseInt(etEdad.getText().toString());
                if(edad <= 0 || edad > 120 ){
                    mensajeError += "Ingrese una edad valida 1 a 120 \n";
                }
            }catch (NumberFormatException e){
                mensajeError += "Ingrese una edad valida 1 a 120 \n";
            }
        }else{
            mensajeError += "Ingrese una edad valida 1 a 120 \n";
        }

        if(mensajeError.length()>0){
            Toast.makeText(this, mensajeError, Toast.LENGTH_SHORT).show();
        }else{
            Formulario nuevoForm = new Formulario();
            nuevoForm.setEdad(Integer.parseInt(etEdad.getText().toString()));
            nuevoForm.setNombre(etNombre.getText().toString());
            nuevoForm.setTelefono(etTelefono.getText().toString());
            nuevoForm.setRut(etRut.getText().toString());

            AlmacenDeFormularios.agregarFormulario(nuevoForm);
            limpiarTextos();
            Toast.makeText(this, "Formulario guardado correctamente", Toast.LENGTH_LONG).show();
        }

       /* if(etNombre.getText().toString().length()>0){
            Formulario nuevoForm = new Formulario();
            nuevoForm.setEdad(Integer.parseInt(etEdad.getText().toString()));
            nuevoForm.setNombre(etNombre.getText().toString());
            nuevoForm.setTelefono(etTelefono.getText().toString());
            nuevoForm.setRut(etRut.getText().toString());

            AlmacenDeFormularios.agregarFormulario(nuevoForm);
            limpiarTextos();
            Toast.makeText(this, "Formulario guardado correctamente", Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(this, "Ingrese Valores", Toast.LENGTH_SHORT).show();
        }*/
    }

    private void limpiarTextos() {
        etRut.setText("");
        etNombre.setText("");
        etEdad.setText("");
        etTelefono.setText("");
        etNombre.requestFocus();
    }

    private boolean isValidarRut(String rut) {

        boolean validacion = false;
        try {
            rut =  rut.toUpperCase();
            rut = rut.replace(".", "");
            rut = rut.replace("-", "");
            int rutAux = Integer.parseInt(rut.substring(0, rut.length() - 1));

            char dv = rut.charAt(rut.length() - 1);

            int m = 0, s = 1;
            for (; rutAux != 0; rutAux /= 10) {
                s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
            }
            if (dv == (char) (s != 0 ? s + 47 : 75)) {
                validacion = true;
            }

        } catch (java.lang.NumberFormatException e) {
        } catch (Exception e) {
        }
        return validacion;
    }
}
